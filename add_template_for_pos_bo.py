from pyzabbix import ZabbixAPI
from datetime import datetime, timedelta
from os import system


def add_hosts(hosts, templates, z, package_size=100):
    # hosts "hosts":[{'hostid': 13456}]
    # templates "templates": [{ 'templateid':"135063"}]
    if z == 'x5':
        z = ZabbixAPI('http://msk-dpro-app351')
        z.login("andrey.gavrilov", "200994")
    elif z == 'head':
        z = ZabbixAPI("http://zabbix-head.x5.ru")
        z.login("andrey.gavrilov", "200994Razor")
    print('Operation: add template')
    print('Number of hosts:', len(hosts))
    print('Templates_ids:')
    for i in range(len(templates)):
        print('{}. {}'.format(i + 1, templates[i]))
    key = input("Start? (y/n)\n")
    if key.upper() == 'Y':
        host_load = round((len(hosts) / package_size) + 0.5)
        host_packages = [hosts[i * package_size:(i + 1) * package_size] for i in range(host_load)]
        host_remaind = 1000
        host_count = 0
        host_ev_time = []
        for i in host_packages:
            print('Готово пакетов', host_count, 'из', len(host_packages))
            print('Осталось ждать:', timedelta(seconds=host_remaind))
            start_time = datetime.timestamp(datetime.now())
            update = z.do_request('template.massadd', {
                          "templates": templates,
                "hosts": i
                })['result']
            finish_time = datetime.timestamp(datetime.now())
            host_ev_time.append(int(finish_time-start_time))
            host_count += 1
            host_remaind = sum(host_ev_time)/len(host_ev_time) * (len(host_packages) - len(host_ev_time))
            system('cls')



def remove_hosts(hosts, templates, z, package_size=100, mode='templateids'):
    # hosts "hostids":[124132412, 123, 124]
    # templates "templates": [31242, 3423, 235]
    # mode templateids or templateids_clear
    if z == 'x5':
        z = ZabbixAPI('http://msk-dpro-app351')
        z.login("andrey.gavrilov", "200994")
    elif z == 'head':
        z = ZabbixAPI("http://zabbix-head.x5.ru")
        z.login("andrey.gavrilov", "200994Razor")
    print('Operation: remove template')
    print('Number of hosts:', len(hosts))
    print('Mode:', mode)
    print('Templates_ids:')
    for i in range(len(templates)):
        print('{}. {}'.format(i+1, templates[i]))
    key = input("Start? (y/n)\n")
    if key.upper() == 'Y':
        host_load = round((len(hosts) / package_size) + 0.5)
        host_packages = [hosts[i * package_size:(i + 1) * package_size] for i in range(host_load)]
        host_remaind = 1000
        host_count = 0
        host_ev_time = []
        for i in host_packages:
            print('Готово пакетов', host_count, 'из', len(host_packages))
            print('Осталось ждать:', timedelta(seconds=host_remaind))
            start_time = datetime.timestamp(datetime.now())
            remove = z.do_request('host.massremove', {
                          "hostids": i,
                            mode: templates
        })['result']
            finish_time = datetime.timestamp(datetime.now())
            host_ev_time.append(int(finish_time-start_time))
            host_count += 1
            host_remaind = sum(host_ev_time)/len(host_ev_time) * (len(host_packages) - len(host_ev_time))
            system('cls')



z = ZabbixAPI('http://msk-dpro-app351')
z.login("andrey.gavrilov", "200994")
bo = z.do_request('host.get', {
    'search': {'name': 'POS*'},
    # 'template_ids': '133936',
    'searchWildcardsEnabled': 'true',
    'output': ['name']
})['result']
# hosts = [i['hostid'] for i in bo]
# remove_hosts(hosts, ['133936'], 'x5', mode='templateids')

hosts = [{'hostid': i['hostid']} for i in bo]
add_hosts(hosts, [{'templateid':"133936"}], 'x5', )
